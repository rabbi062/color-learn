using System;
using UnityEngine;
using UnityEngine.UI;

namespace FXnRXn
{
    public class CinematicManager : MonoBehaviour
    {
        #region Variable

        [SerializeField] private Button skipButton;

        #endregion

        #region Function

        private void Start()
        {
            if (PlayerPrefs.GetInt(GlobalData.gameFirstTimeOpen) == 0 || !PlayerPrefs.HasKey(GlobalData.gameFirstTimeOpen))
            {
                skipButton.gameObject.SetActive(false);
            }
            else
            {
                skipButton.gameObject.SetActive(true);
            }
            
            skipButton.onClick.AddListener(() =>
            {
                if (LevelManager.singleton != null)
                {
                    LevelManager.singleton.LoadLevel("Menu");
                }
            });
        }

        #endregion

    }
}

