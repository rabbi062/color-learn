using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

namespace FXnRXn
{
	public class AuthenticationManagerSC : UIBaseSC
	{
   		#region Variable
        
        // PUBLIC
        [SerializeField] private bool clearPlayerPrefs;
        
        [Header("Authentication Service :")] 
        //[SerializeField] private Button loginCancelButton;
        //[SerializeField] private Button errorOKButton;
        [SerializeField] private Button LoginButton;
        
        [Header("Input :")]
        [SerializeField] private TMP_InputField password;
        [SerializeField] private TMP_InputField username;

        [Header("What Data Need :")]
        [SerializeField] private GetPlayerCombinedInfoRequestParams InfoRequestParams;

        // PRIVATE
        private PlayFabAuthService _AuthService = PlayFabAuthService.Instance;
        private readonly string _loginPanel = "Auth";

		#endregion

		#region Function

		private void Awake()
		{
			MDisableAllScreens();
			
			//Reset
			if (clearPlayerPrefs)
			{
				_AuthService.UnlinkSilentAuth();
				_AuthService.ClearRememberMe();
				_AuthService.AuthType = Authtypes.None;
				clearPlayerPrefs = false;
			}
			
			// Remember me
			if (PlayerPrefs.GetInt(GlobalData.gameFirstTimeOpen) == 0)
			{
				_AuthService.RememberMe = false;
				PlayerPrefs.SetInt(GlobalData.gameFirstTimeOpen, 1);
			}

		}

		private void Start()
		{
			// At start show auth panel
			if (!_AuthService.RememberMe)
			{
				CheckInternetConnection();
			}
			
			ButtonPress();
			
			//
			PlayFabAuthService.OnLoginSuccess += OnLoginSuccess;
			PlayFabAuthService.OnPlayFabError += OnPlayFaberror;
			

			LoginButton.onClick.AddListener(OnLoginClicked);
			//RegisterButton.onClick.AddListener(OnRegisterButtonClicked);
			//CancelRegisterButton.onClick.AddListener(OnCancelRegisterButtonClicked);


			_AuthService.InfoRequestParams = InfoRequestParams;
			//Start the authentication process.
			_AuthService.Authenticate();
		}


		public void LogOut()
		{
			_AuthService.UnlinkSilentAuth();
			_AuthService.ClearRememberMe();
			_AuthService.AuthType = Authtypes.None;
			
			Application.Quit();
		}
		
		
		
		void CheckInternetConnection()
		{
			if (Application.internetReachability == NetworkReachability.NotReachable) // No Internet
			{
				
			}
			else // Internet has
			{
				ShowMenu(_loginPanel);
				
			}
		}

		void ButtonPress()
		{
			
		}
		
		private void OnLoginSuccess(PlayFab.ClientModels.LoginResult result)
		{
			
			MDisableAllScreens();
			var request = new GetAccountInfoRequest();
			PlayFabClientAPI.GetAccountInfo(request, result =>
				{
					//Debug.Log("username : " + result.AccountInfo.Username);
					
					
				},
				error =>
				{
					Debug.LogError(error.GenerateErrorReport());
				});
			
			if (_AuthService.Username != null)
			{
				var req = new UpdateUserTitleDisplayNameRequest 
				{
					DisplayName = _AuthService.Username,
				}; 
				PlayFabClientAPI.UpdateUserTitleDisplayName(req, OnDisplayNameUpdated, error =>
				{
					Debug.LogError(error.GenerateErrorReport());
				});
			}

		}

		void OnDisplayNameUpdated(UpdateUserTitleDisplayNameResult result)
		{
			if (result.DisplayName != null)
			{
				PlayerPrefs.SetString(GlobalData.playerUsername, result.DisplayName);
			}

			if (UIMenuSC.singleton != null)
			{
				UIMenuSC.singleton.ShowUserName();
			}
			
		}
		
		private void OnPlayFaberror(PlayFabError error)
		{
			//Error List
			switch (error.Error)
			{
				case PlayFabErrorCode.InvalidEmailAddress:
				case PlayFabErrorCode.InvalidPassword:
				case PlayFabErrorCode.InvalidEmailOrPassword:
					Debug.Log("Invalid Email or Password");
					break;

				case PlayFabErrorCode.AccountNotFound:
					//ShowMenu(registerPanel);
					OnRegisterButtonClicked();
					return;
				case PlayFabErrorCode.DuplicateUsername:
					Debug.Log("Duplicate UserName");
					break;
				default:
					Debug.Log(error.GenerateErrorReport());
					break;
                
			}
			
			
			Debug.Log(error.Error);
			Debug.LogError(error.GenerateErrorReport());
		}
		
		
		private void OnLoginClicked()
		{
			_AuthService.RememberMe = true;
			CloseMenu(_loginPanel);
			string email = username.text + "@gmail.com";
			
			_AuthService.Email = email.ToString();
			_AuthService.Password = password.text;
			_AuthService.Authenticate(Authtypes.EmailAndPassword);
			
		}

		private void OnRegisterButtonClicked()
		{
			string email = username.text + "@gmail.com";
			_AuthService.Email = email.ToString();
			_AuthService.Username = username.text;
			_AuthService.Password = password.text;
			_AuthService.Authenticate(Authtypes.RegisterPlayFabAccount);
			
			
		}
		
		private void OnCancelRegisterButtonClicked()
		{
			//Reset all forms
			username.text = string.Empty;
			password.text = string.Empty;
			username.text = string.Empty;
			//Show panels
		}
		
		
		
		
		
		
		
		#endregion
	}
}
