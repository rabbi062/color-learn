using System;
using System.Collections;
using System.Collections.Generic;
using FXnRXn.Manager;
using UnityEngine;



namespace FXnRXn
{
	public class CsParkingCarController : MonoBehaviour
	{
   		#region Variable
        //It’s not Right Parking Zone
        
        [Header("--- Setting :")]
        [SerializeField]private GameObject errorPanel;
        [SerializeField]private GameObject winPanel;
        
        [Header("--- Sound :")]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip itsNotParkingZone;

		#endregion

		#region Function

		private void Start()
		{
			EventManager.TriggerEvent(GlobalData.timerState, true);
		}

		public void VehicleTriggerEnter(GameObject coll)
		{
			if (coll.CompareTag("Main"))
			{
				GameOver();
			}
			else
			{
				errorPanel.SetActive(true);
				_audioSource.PlayOneShot(itsNotParkingZone);
				this.Wait(4f, () =>
				{
					errorPanel.SetActive(false);
				});
			}
			
		}
		
		void GameOver()
		{
			EventManager.TriggerEvent(GlobalData.timerState, false);
			winPanel.SetActive(true);
		}

		#endregion
	}
}
