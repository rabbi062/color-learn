using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace FXnRXn
{
	
	
	
    public class MoveBothPlayerSecondarySC : MonoBehaviour
    {
	#region Variable

	public Animator _anim;
	private readonly string walkAnimParam = "Walk";
	private SecondaryColorManagerSC _manager;

	#endregion

	#region Function

	private void Start()
	{
		_manager = Object.FindObjectOfType<SecondaryColorManagerSC>();
	}


	public void OrangeJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnOrangeJarReached = true;
	}
	
	public void GreenJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnGreenJarReached = true;
	}
	
	public void VioletJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnVioletJarReached = true;
	}

	#endregion

    }
}


