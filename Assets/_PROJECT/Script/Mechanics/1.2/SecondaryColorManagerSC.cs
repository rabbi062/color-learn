using System.Collections;
using FXnRXn.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace FXnRXn
{
    public class SecondaryColorManagerSC : MonoBehaviour
    {
	#region Variable
	
	[Header("--- Setting :")] 
        
	[SerializeField]private GameObject[] colorChilds;
	[SerializeField]private GameObject allColor;
	[SerializeField]private GameObject winPanel;
	[SerializeField] private Button _tapText;
	
	[Header("--- Sound :")]
	[SerializeField] private AudioSource _audioSource;
	[SerializeField] private AudioClip orangeIsSecondaryAudio;
	[SerializeField] private AudioClip greenIsSecondaryAudio;
	[SerializeField] private AudioClip violetIsSecondaryAudio;
	[SerializeField] private AudioClip ogvIsSecondaryAudio;

        
	private bool o, oo = false;
	private bool g, gg = false;
	private bool v, vv = false;

	[HideInInspector]
	public bool OnOrangeJarReached = false;
	[HideInInspector]
	public bool OnGreenJarReached = false;
	[HideInInspector]
	public bool OnVioletJarReached = false;
	
	#endregion

	#region Function

	private void Start()
	{
		allColor.SetActive(false);
		winPanel.SetActive(false);
		foreach (var color in colorChilds)
		{
			color.SetActive(false);
		}
		colorChilds[0].SetActive(true);
		
		_tapText.gameObject.SetActive(false);
		EventManager.TriggerEvent(GlobalData.timerState, true);
		
		_tapText.onClick.RemoveAllListeners();
			_tapText.onClick.AddListener(() =>
			{
				if (o && !oo)
				{
					colorChilds[0].GetComponent<Animator>().SetTrigger("Jar");
					this.Wait(2f, () =>
					{
						_audioSource.PlayOneShot(orangeIsSecondaryAudio);
						this.Wait(4f,()=>
						{
							if (colorChilds[0] == null 
							    || colorChilds[1] == null 
							    || colorChilds[2] == null) return;
							
							colorChilds[0].SetActive(false);
							colorChilds[1].SetActive(true);
							colorChilds[2].SetActive(false);

							if (o && g && v)
							{
								StartCoroutine(ShowCompletePanel());
							}
							
						});
					});
					oo = true;
					_tapText.gameObject.SetActive(false);
				}
				
				if (g && !gg)
				{
					colorChilds[1].GetComponent<Animator>().SetTrigger("Jar");
					this.Wait(2f, () =>
					{
						_audioSource.PlayOneShot(greenIsSecondaryAudio);
						this.Wait(4f,()=>
						{
							if (colorChilds[0] == null 
							    || colorChilds[1] == null 
							    || colorChilds[2] == null) return;
							
							colorChilds[0].SetActive(false);
							colorChilds[1].SetActive(false);
							colorChilds[2].SetActive(true);

							if (o && g && v)
							{
								StartCoroutine(ShowCompletePanel());
							}
							
						});
					});
					gg = true;
					_tapText.gameObject.SetActive(false);
				}
				
				if (v && !vv)
				{
					colorChilds[2].GetComponent<Animator>().SetTrigger("Jar");
					this.Wait(2f, () =>
					{
						_audioSource.PlayOneShot(violetIsSecondaryAudio);
						this.Wait(4f,()=>
						{
							if (colorChilds[0] == null 
							    || colorChilds[1] == null 
							    || colorChilds[2] == null) return;
							
							colorChilds[0].SetActive(false);
							colorChilds[1].SetActive(false);
							colorChilds[2].SetActive(false);

							if (o && g && v)
							{
								StartCoroutine(ShowCompletePanel());
							}
							
						});
					});
					vv = true;
					_tapText.gameObject.SetActive(false);
				}


			});
			
		
			
	}
	
	
	
	
	private void Update()
	{
		if (OnOrangeJarReached && o == false)
		{
			_tapText.gameObject.SetActive(true);
				
			o = true;
		}
			
		if (OnGreenJarReached && g == false)
		{
			_tapText.gameObject.SetActive(true);
				
			g = true;
		}
			
		if (OnVioletJarReached && v == false)
		{
			_tapText.gameObject.SetActive(true);
				
			v = true;
		}
	}


	IEnumerator ShowCompletePanel()
	{
		allColor.SetActive(true);
		EventManager.TriggerEvent(GlobalData.timerState, false);
		_audioSource.PlayOneShot(ogvIsSecondaryAudio);
			
		yield return new WaitForSeconds(4f);
		allColor.SetActive(false);
		winPanel.SetActive(true);
	}
	
	#endregion
        
    }
}


