using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FXnRXn
{
	public class AlphabetSpawnController : MonoBehaviour
	{
   		#region Variable

        [SerializeField] private List<GameObject> alphabetSceneName = new List<GameObject>();

        #endregion

        #region Function

        private void Awake()
        {
	        if (!PlayerPrefs.HasKey(GlobalData.alphabetScene))
	        {
		        PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.alphabetScene, 1);
	        }


	        int temp = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.alphabetScene, 0);
	        GameObject obj = Instantiate(alphabetSceneName[temp - 1]);
        }

        #endregion
	}
}
