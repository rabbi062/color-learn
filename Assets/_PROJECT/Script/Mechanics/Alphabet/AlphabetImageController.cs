using System.Collections;
using System.Collections.Generic;
using FXnRXn.Manager;
using UnityEngine;
using UnityEngine.UI;


namespace FXnRXn
{
	public class AlphabetImageController : MonoBehaviour
	{
   		#region Variable
        [Header("--- Setting :")] 
        [SerializeField] private Sprite defaultImage; 
        [SerializeField] private Sprite FinalImage;
        [SerializeField] private Image image;
        
        [SerializeField]private GameObject errorPanel;
        [SerializeField]private GameObject winPanel;
        
        [Header("--- Sound :")]
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _startAudio;
        [SerializeField] private AudioClip youAreWrongAudio;

		#endregion

		#region Function
		private void OnEnable()
		{
			EventManager.TriggerEvent(GlobalData.timerState, true);
			image.sprite = defaultImage;
			
			this.Wait(.5f, () =>
			{
				_audioSource.PlayOneShot(_startAudio);
			});
		}
		
		public void ATriggerEnter(GameObject coll)
		{
			if (coll.CompareTag("Main"))
			{
				image.sprite = FinalImage;
				string objName = PlayerPrefs.GetString("CurrentTriggerToolName");
				GameObject obj = GameObject.Find(objName);
				Destroy(obj);
				
				
				this.Wait(1f, () =>
				{
					GameOver();
				});

			}
		}
		
		public void OtherAlphabetTriggerEnter(GameObject coll)
		{
			if (coll.CompareTag("Main"))
			{
				errorPanel.SetActive(true);
				_audioSource.PlayOneShot(youAreWrongAudio);
				this.Wait(3f, () =>
				{
					errorPanel.SetActive(false);
				});
			}
		}



		void GameOver()
		{
			EventManager.TriggerEvent(GlobalData.timerState, false);
			winPanel.SetActive(true);
		}

		#endregion
	}
}
