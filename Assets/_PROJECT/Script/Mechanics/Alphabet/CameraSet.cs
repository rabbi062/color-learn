using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FXnRXn
{
	public class CameraSet : MonoBehaviour
	{
   		#region Variable

        private Camera cam;

		#endregion

		#region Function

		private void OnEnable()
		{
			cam = Camera.main;
			GetComponent<Canvas>().worldCamera = cam;
		}

		#endregion
	}
}
