using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace FXnRXn
{
	public enum DragAlphabetType
	{
		A,OtherAlphabet
	}
	
	
	public class DragAlphabet : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
	{
   		#region Variable
        
        public DragAlphabetType dragAlphabetType;
        public GameObject onCollision;
	
	
        [HideInInspector()]
        public  bool bDrag = false;
	
        Vector3 diffPos = new Vector3(0,0,0);
        private Vector3 StartPosition;
        Vector3 offPos = Vector3.zero;
        PointerEventData pointerEventData;
        bool bMovingBack = false;
	
        float x;
        float y;
        private Camera cam;

		#endregion

		#region Function
		private void Awake()
		{
			bDrag = false;
		}

		private void OnEnable()
	{
		cam = Camera.main;
		StartPosition  = transform.position;
	}

		private void Update()
	{
		if(   bDrag )
		{
			x = Input.mousePosition.x;
			y = Input.mousePosition.y;
 
			Vector3 posM = cam.ScreenToWorldPoint(new Vector3(x ,y,5f) ) - offPos;
			transform.position =  Vector3.Lerp (transform.position, posM  , 10 * Time.deltaTime)  ;
		}
	}


		public void OnBeginDrag(PointerEventData eventData)
	{
		if(bMovingBack) return;
		pointerEventData = eventData;

		if (!bDrag)
		{
			bDrag = true;
			diffPos =transform.position - cam.ScreenToWorldPoint(Input.mousePosition)   ;
			diffPos = new Vector3(diffPos.x,diffPos.y,0);
			
			
		}
	}

		public void OnDrag(PointerEventData eventData)
	{
		
	}

		public void OnEndDrag(PointerEventData eventData)
	{
		if (bDrag)
		{
			bDrag = false;
			StartCoroutine("MoveBack" );
		}
	}

		IEnumerator MoveBack()
	{
		if (!bMovingBack)
		{
			bMovingBack = true;
			yield return new WaitForEndOfFrame( );
			
			float pom = 0;
			Vector3 positionS = transform.position;
			while(pom<1 )
			{ 
				pom+=Time.fixedDeltaTime*2;
				transform.position = Vector3.Lerp(positionS, StartPosition,pom);
				yield return new WaitForFixedUpdate( );
			}
			
			transform.position = StartPosition;
			bMovingBack = false;
		}
	}
	
	
		#region Collision Callbacks

		void OnCollisionEnter2D(Collision2D coll)
	{
		PlayerPrefs.SetString ("CurrentTriggerToolName",this.gameObject.name);
		onCollision.BroadcastMessage (dragAlphabetType.ToString () + "CollisionEnter",
			coll.gameObject,
			SendMessageOptions.DontRequireReceiver);
	}

		void OnCollisionStay2D(Collision2D coll)
	{
		onCollision.BroadcastMessage (dragAlphabetType.ToString () + "CollisionStay",
			coll.gameObject,
			SendMessageOptions.DontRequireReceiver);
	}

		void OnCollisionExit2D(Collision2D coll)
	{
		onCollision.BroadcastMessage (dragAlphabetType.ToString () + "CollisionExit",
			coll.gameObject,
			SendMessageOptions.DontRequireReceiver);
	}
		#endregion
        
		#region Trigger Callbacks
	
		void OnTriggerEnter2D(Collider2D coll)
	{
		PlayerPrefs.SetString ("CurrentTriggerToolName",this.gameObject.name);
		onCollision.BroadcastMessage (dragAlphabetType.ToString () + "TriggerEnter",
			coll.gameObject,
			SendMessageOptions.DontRequireReceiver);
		
	}
	
		void OnTriggerStay2D(Collider2D coll)
	{
		
		onCollision.BroadcastMessage (dragAlphabetType.ToString () + "TriggerStay",
			coll.gameObject,
			SendMessageOptions.DontRequireReceiver);
	}
	
		void OnTriggerExit2D(Collider2D coll)
	{
		
		onCollision.BroadcastMessage (dragAlphabetType.ToString () + "TriggerExit",
			coll.gameObject,
			SendMessageOptions.DontRequireReceiver);
	}
		#endregion
		
		
		
		#endregion
	}
	
}
