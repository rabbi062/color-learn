using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace FXnRXn
{
	public enum PrimaryType
	{
		Red,
		Blue,
		yellow
	}
	
	
    public class MovePlayerTowards : MonoBehaviour
    {
	#region Variable

	[SerializeField] private Vector3 _endPos;
	[SerializeField] private float _speed;
	[SerializeField] private PrimaryType _primaryType;


	public Animator _anim;
	private readonly string walkAnimParam = "Walk";
	private PrimaryColorManagerSC _manager;

	#endregion

	#region Function

	private void Start()
	{
		_manager = Object.FindObjectOfType<PrimaryColorManagerSC>();
		
		
	}

	private void OnEnable()
	{
		_anim.SetBool(walkAnimParam, true);
	}

	private void Update()
	{
		if (transform.position != _endPos)
		{
			float step = _speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, _endPos, step);
		}
		else
		{
			switch (_primaryType)
			{
				case PrimaryType.Red:
					_manager.OnRedJarReached = true;
					_anim.SetBool(walkAnimParam, false);
					break;
				case PrimaryType.Blue:
					_manager.OnBlueJarReached = true;
					_anim.SetBool(walkAnimParam, false);
					break;
				case PrimaryType.yellow:
					_manager.OnYellowJarReached = true;
					_anim.SetBool(walkAnimParam, false);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}

	#endregion

    }
}


