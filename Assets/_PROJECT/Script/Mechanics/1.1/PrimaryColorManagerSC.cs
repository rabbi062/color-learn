using System;
using System.Collections;
using System.Collections.Generic;
using FXnRXn.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace FXnRXn
{
	public class PrimaryColorManagerSC : MonoBehaviour
	{
   		#region Variable

        [Header("--- Setting :")] 
        
        [SerializeField]private GameObject[] colorChilds;
        [SerializeField]private GameObject allColor;
        [SerializeField]private GameObject winPanel;
        
        
        [SerializeField] private Button _tapText;

        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip redIsPrimaryAudio;
        [SerializeField] private AudioClip blueIsPrimaryAudio;
        [SerializeField] private AudioClip yellowIsPrimaryAudio;
        [SerializeField] private AudioClip rbyIsPrimaryAudio;

        
        private bool r, rr = false;
        private bool b, bb = false;
        private bool y, yy = false;

		[HideInInspector]
        public bool OnRedJarReached = false;
        [HideInInspector]
        public bool OnBlueJarReached = false;
        [HideInInspector]
        public bool OnYellowJarReached = false;

		#endregion

		#region Function

		private void Start()
		{
			allColor.SetActive(false);
			winPanel.SetActive(false);
			foreach (var color in colorChilds)
			{
				color.SetActive(false);
			}
			colorChilds[0].SetActive(true);
			
			_tapText.gameObject.SetActive(false);
			EventManager.TriggerEvent(GlobalData.timerState, true);
			
			_tapText.onClick.RemoveAllListeners();
			_tapText.onClick.AddListener(() =>
			{
				if (r && !rr)
				{
					colorChilds[0].GetComponent<Animator>().SetTrigger("Jar");
					this.Wait(2f, () =>
					{
						_audioSource.PlayOneShot(redIsPrimaryAudio);
						this.Wait(4f,()=>
						{
							if (colorChilds[0] == null 
							    || colorChilds[1] == null 
							    || colorChilds[2] == null) return;
							
							colorChilds[0].SetActive(false);
							colorChilds[1].SetActive(true);
							colorChilds[2].SetActive(false);

							if (r && b && y)
							{
								StartCoroutine(ShowCompletePanel());
							}
							
						});
					});
					rr = true;
					_tapText.gameObject.SetActive(false);
				}
				
				
				if (b && !bb)
				{
					colorChilds[1].GetComponent<Animator>().SetTrigger("Jar");
					this.Wait(2f, () =>
					{
						_audioSource.PlayOneShot(blueIsPrimaryAudio);
						this.Wait(4f,()=>
						{
							if (colorChilds[0] == null 
							    || colorChilds[1] == null 
							    || colorChilds[2] == null) return;
							
							colorChilds[0].SetActive(false);
							colorChilds[1].SetActive(false);
							colorChilds[2].SetActive(true);

							if (r && b && y)
							{
								StartCoroutine(ShowCompletePanel());
							}
							
						});
					});
					bb = true;
					_tapText.gameObject.SetActive(false);
				}
				
				if (y && !yy)
				{
					colorChilds[2].GetComponent<Animator>().SetTrigger("Jar");
					this.Wait(2f, () =>
					{
						_audioSource.PlayOneShot(yellowIsPrimaryAudio);
						this.Wait(4f,()=>
						{
							if (colorChilds[0] == null 
							    || colorChilds[1] == null 
							    || colorChilds[2] == null) return;
							
							colorChilds[0].SetActive(false);
							colorChilds[1].SetActive(false);
							colorChilds[2].SetActive(false);

							if (r && b && y)
							{
								StartCoroutine(ShowCompletePanel());
							}
							
						});
					});
					yy = true;
					_tapText.gameObject.SetActive(false);
				}
				
				
				
				
				
			});
			
			
			// 
			//
			// this.Wait(6f, () =>
			// {
			// 	EventManager.TriggerEvent(GlobalData.timerState, false);
			// });
		}


		private void Update()
		{
			if (OnRedJarReached && r == false)
			{
				_tapText.gameObject.SetActive(true);
				
				r = true;
			}
			
			if (OnBlueJarReached && b == false)
			{
				_tapText.gameObject.SetActive(true);
				
				b = true;
			}
			
			if (OnYellowJarReached && y == false)
			{
				_tapText.gameObject.SetActive(true);
				
				y = true;
			}
		}


		IEnumerator ShowCompletePanel()
		{
			allColor.SetActive(true);
			EventManager.TriggerEvent(GlobalData.timerState, false);
			_audioSource.PlayOneShot(rbyIsPrimaryAudio);
			
			yield return new WaitForSeconds(4f);
			allColor.SetActive(false);
			winPanel.SetActive(true);
		}
		
		

		#endregion
	}
}
