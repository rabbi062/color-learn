using System.Collections;
using System.Collections.Generic;
using FXnRXn.Manager;
using TMPro;
using UnityEngine;



namespace FXnRXn
{
	public class Timer : MonoBehaviour
	{
   		#region Variable
        private float timeRemaining = 0;
        private bool timerIsRunning = false;
        [SerializeField] private TMP_Text timerText;

        #endregion

        #region Function
        
        private void OnEnable()
        {
	        EventManager.StartListening(GlobalData.timerState, SetTimerBool);
        }

        private void OnDisable()
        {
	        EventManager.StopListening(GlobalData.timerState, SetTimerBool);
        }
        
        
        
        void Update()
        {
	        if (timerIsRunning)
	        {
		        timeRemaining += Time.deltaTime;
		        DisplayTime(timeRemaining);
	        }
        }
        
        public void SetTimerBool(bool t)
        {
	        if (t)
	        {
		        timerIsRunning = true;
	        }
	        else
	        {
		        timerIsRunning = false;
		        PlayerPrefs.SetFloat(GlobalData.timer, timeRemaining);
		        timeRemaining = 0;
	        }
        }
        
        void DisplayTime(float timeToDisplay)
        {
	        timeToDisplay += 1;

	        float minutes = Mathf.FloorToInt(timeToDisplay / 60); 
	        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

	        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }

        #endregion
	}
}
