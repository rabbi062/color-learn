using System;
using System.Collections;
using FXnRXn.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace FXnRXn
{
    public class CsTertiaryColorManager : MonoBehaviour
    {
	#region Variable
	
	[Header("--- Setting :")] 
        
	[SerializeField]private GameObject[] colorChilds;
	[SerializeField]private GameObject allColor;
	[SerializeField]private GameObject winPanel;
	[SerializeField] private Button _tapText;
	
	[Header("--- Sound :")]
	[SerializeField] private AudioSource _audioSource;
	[SerializeField] private AudioClip yelloworangeIsTertiaryAudio;
	[SerializeField] private AudioClip redorangeIsTertiaryAudio;
	[SerializeField] private AudioClip redvioletIsTertiaryAudio;
	[SerializeField] private AudioClip bluevioletIsTertiaryAudio;
	[SerializeField] private AudioClip bluegreenIsTertiaryAudio;
	[SerializeField] private AudioClip yellowgreenIsTertiaryAudio;
	[SerializeField] private AudioClip allIsTertiaryAudio;
	
	
	
	private bool yo, yoyo = false;
	private bool ro, roro = false;
	private bool rv, rvrv = false;
	private bool bv, bvbv = false;
	private bool bg, bgbg = false;
	private bool yg, ygyg = false;
	
	
	[HideInInspector]
	public bool OnYellowOrangeJarReached = false;
	[HideInInspector]
	public bool OnRedOrangeJarReached = false;
	[HideInInspector]
	public bool OnRedVioletJarReached = false;
	[HideInInspector]
	public bool OnBlueVioletJarReached = false;
	[HideInInspector]
	public bool OnBlueGreenJarReached = false;
	[HideInInspector]
	public bool OnYellowGreenJarReached = false;
	
	#endregion

	#region Function

	private void Start()
	{
		allColor.SetActive(false);
		winPanel.SetActive(false);
		foreach (var color in colorChilds)
		{
			color.SetActive(false);
		}
		colorChilds[0].SetActive(true);
		
		_tapText.gameObject.SetActive(false);
		EventManager.TriggerEvent(GlobalData.timerState, true);
		
		
		_tapText.onClick.RemoveAllListeners();
		_tapText.onClick.AddListener(() =>
		{
			
			if (yo && !yoyo)
			{
				colorChilds[0].GetComponent<Animator>().SetTrigger("Jar");
				this.Wait(2f, () =>
				{
					_audioSource.PlayOneShot(yelloworangeIsTertiaryAudio);
					this.Wait(4f,()=>
					{
						if (colorChilds[0] == null || colorChilds[1] == null ||
						    colorChilds[2] == null || colorChilds[3] == null ||
						    colorChilds[4] == null || colorChilds[5] == null) return;
							
						foreach (var color in colorChilds)
						{
							color.SetActive(false);
						}
						colorChilds[1].SetActive(true);

						if (yo && ro && rv && bv && bg && yg)
						{
							StartCoroutine(ShowCompletePanel());
						}
							
					});
				});
				yoyo = true;
				_tapText.gameObject.SetActive(false);
			}
			
			if (ro && !roro)
			{
				colorChilds[1].GetComponent<Animator>().SetTrigger("Jar");
				this.Wait(2f, () =>
				{
					_audioSource.PlayOneShot(redorangeIsTertiaryAudio);
					this.Wait(4f,()=>
					{
						if (colorChilds[0] == null || colorChilds[1] == null ||
						    colorChilds[2] == null || colorChilds[3] == null ||
						    colorChilds[4] == null || colorChilds[5] == null) return;
							
						foreach (var color in colorChilds)
						{
							color.SetActive(false);
						}
						colorChilds[2].SetActive(true);

						if (yo && ro && rv && bv && bg && yg)
						{
							StartCoroutine(ShowCompletePanel());
						}
							
					});
				});
				roro = true;
				_tapText.gameObject.SetActive(false);
			}
			
			if (rv && !rvrv)
			{
				colorChilds[2].GetComponent<Animator>().SetTrigger("Jar");
				this.Wait(2f, () =>
				{
					_audioSource.PlayOneShot(redvioletIsTertiaryAudio);
					this.Wait(4f,()=>
					{
						if (colorChilds[0] == null || colorChilds[1] == null ||
						    colorChilds[2] == null || colorChilds[3] == null ||
						    colorChilds[4] == null || colorChilds[5] == null) return;
							
						foreach (var color in colorChilds)
						{
							color.SetActive(false);
						}
						colorChilds[3].SetActive(true);

						if (yo && ro && rv && bv && bg && yg)
						{
							StartCoroutine(ShowCompletePanel());
						}
							
					});
				});
				rvrv = true;
				_tapText.gameObject.SetActive(false);
			}
			
			if (bv && !bvbv)
			{
				colorChilds[3].GetComponent<Animator>().SetTrigger("Jar");
				this.Wait(2f, () =>
				{
					_audioSource.PlayOneShot(bluevioletIsTertiaryAudio);
					this.Wait(4f,()=>
					{
						if (colorChilds[0] == null || colorChilds[1] == null ||
						    colorChilds[2] == null || colorChilds[3] == null ||
						    colorChilds[4] == null || colorChilds[5] == null) return;
							
						foreach (var color in colorChilds)
						{
							color.SetActive(false);
						}
						colorChilds[4].SetActive(true);

						if (yo && ro && rv && bv && bg && yg)
						{
							StartCoroutine(ShowCompletePanel());
						}
							
					});
				});
				bvbv = true;
				_tapText.gameObject.SetActive(false);
			}
			
			if (bg && !bgbg)
			{
				colorChilds[4].GetComponent<Animator>().SetTrigger("Jar");
				this.Wait(2f, () =>
				{
					_audioSource.PlayOneShot(bluegreenIsTertiaryAudio);
					this.Wait(4f,()=>
					{
						if (colorChilds[0] == null || colorChilds[1] == null ||
						    colorChilds[2] == null || colorChilds[3] == null ||
						    colorChilds[4] == null || colorChilds[5] == null) return;
							
						foreach (var color in colorChilds)
						{
							color.SetActive(false);
						}
						colorChilds[5].SetActive(true);

						if (yo && ro && rv && bv && bg && yg)
						{
							StartCoroutine(ShowCompletePanel());
						}
							
					});
				});
				bgbg = true;
				_tapText.gameObject.SetActive(false);
			}
			
			if (yg && !ygyg)
			{
				colorChilds[5].GetComponent<Animator>().SetTrigger("Jar");
				this.Wait(2f, () =>
				{
					_audioSource.PlayOneShot(yellowgreenIsTertiaryAudio);
					this.Wait(4f,()=>
					{
						if (colorChilds[0] == null || colorChilds[1] == null ||
						    colorChilds[2] == null || colorChilds[3] == null ||
						    colorChilds[4] == null || colorChilds[5] == null) return;
							
						foreach (var color in colorChilds)
						{
							color.SetActive(false);
						}

						if (yo && ro && rv && bv && bg && yg)
						{
							StartCoroutine(ShowCompletePanel());
						}
							
					});
				});
				ygyg = true;
				_tapText.gameObject.SetActive(false);
			}
			
			
			
			
		});
		
		
		
		
		
	}
	
	
	
	private void Update()
	{
		if (OnYellowOrangeJarReached && yo == false)
		{
			_tapText.gameObject.SetActive(true);
				
			yo = true;
		}
			
		if (OnRedOrangeJarReached && ro == false)
		{
			_tapText.gameObject.SetActive(true);
				
			ro = true;
		}
			
		if (OnRedVioletJarReached && rv == false)
		{
			_tapText.gameObject.SetActive(true);
				
			rv = true;
		}
		
		if (OnBlueVioletJarReached && bv == false)
		{
			_tapText.gameObject.SetActive(true);
				
			bv = true;
		}
		
		if (OnBlueGreenJarReached && bg == false)
		{
			_tapText.gameObject.SetActive(true);
				
			bg = true;
		}
		
		if (OnYellowGreenJarReached && yg == false)
		{
			_tapText.gameObject.SetActive(true);
				
			yg = true;
		}
	}


	IEnumerator ShowCompletePanel()
	{
		allColor.SetActive(true);
		EventManager.TriggerEvent(GlobalData.timerState, false);
		_audioSource.PlayOneShot(allIsTertiaryAudio);
			
		yield return new WaitForSeconds(9f);
		allColor.SetActive(false);
		winPanel.SetActive(true);
	}
	
	
	
	
	

	#endregion
        
    }
}


