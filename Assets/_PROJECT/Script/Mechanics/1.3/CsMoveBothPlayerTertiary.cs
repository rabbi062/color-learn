using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace FXnRXn
{
    public class CsMoveBothPlayerTertiary : MonoBehaviour
    {
	#region Variable
	
	public Animator _anim;
	private readonly string walkAnimParam = "Walk";
	private CsTertiaryColorManager _manager;
	
	#endregion

	#region Function

	private void Start()
	{
		_manager = Object.FindObjectOfType<CsTertiaryColorManager>();
	}

	void Yellow_OrangeJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnYellowOrangeJarReached = true;
	}
	
	void Red_OrangeJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnRedOrangeJarReached = true;
	}
	
	void Red_VioletJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnRedVioletJarReached = true;
	}
	
	void Blue_VioletJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnBlueVioletJarReached = true;
	}
	
	void Blue_GreenJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnBlueGreenJarReached = true;
	}
	
	void Yellow_GreenJarWalkEnd()
	{
		_anim.SetTrigger(walkAnimParam);
		_manager.OnYellowGreenJarReached = true;
	}
	
	

	#endregion
        
    }
}


