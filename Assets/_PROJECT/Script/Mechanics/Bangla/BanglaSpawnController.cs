using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FXnRXn
{
	public class BanglaSpawnController : MonoBehaviour
	{
   		#region Variable
   		
        [SerializeField] private List<GameObject> banglaSceneName = new List<GameObject>();

		#endregion

		#region Function
		
		private void Awake()
		{
			if (!PlayerPrefs.HasKey(GlobalData.banglaScene))
			{
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.banglaScene, 1);
			}


			int temp = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.banglaScene, 0);
			GameObject obj = Instantiate(banglaSceneName[temp - 1]);
		}

		#endregion
	}
}
