using UnityEngine;

namespace FXnRXn
{
    public class UIPaintingWithColorSC : MonoBehaviour
    {
	#region Variable
	[Header("Alphabet :")] 
	[SerializeField] private GameObject _dim_Alphabet;
	[SerializeField] private GameObject _lock_Alphabet;

	[Header("Bangla :")] 
	[SerializeField] private GameObject _dark_Bangla;
	[SerializeField] private GameObject _lock_Bangla;

	[Header("Drawing And Painting :")] 
	[SerializeField] private GameObject _dark_DrawingAndPainting;
	[SerializeField] private GameObject _lock_DrawingAndPainting;
	
	#endregion

	#region Function
	
	private void OnEnable()
	{
		LevelStatus();
		
	}

	void LevelStatus()
	{
		if (PlayerPrefs.GetInt(GlobalData.paintingWithColor_Alphabet) == 1)
		{
			_dim_Alphabet.SetActive(false);
			_lock_Alphabet.SetActive(false);
		}

		if (PlayerPrefs.GetInt(GlobalData.paintingWithColor_Bangla) == 1)
		{
			_dark_Bangla.SetActive(false);
			_lock_Bangla.SetActive(false);
		}
		else
		{
			_dark_Bangla.SetActive(true);
			_lock_Bangla.SetActive(true);
		}
		
		if (PlayerPrefs.GetInt(GlobalData.paintingWithColor_DrawingAndPainting) == 1)
		{
			_dark_DrawingAndPainting.SetActive(false);
			_lock_DrawingAndPainting.SetActive(false);
		}
		else
		{
			_dark_DrawingAndPainting.SetActive(true);
			_lock_DrawingAndPainting.SetActive(true);
		}
	}
	
	#endregion
        
    }
}


