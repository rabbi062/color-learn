using UnityEngine;

namespace FXnRXn
{
    public class UILowerLevelParkingCarSC : MonoBehaviour
    {
	#region Variable
	[Header("Rickshaw :")] 
	[SerializeField] private GameObject _dim_Rickshaw;
	[SerializeField] private GameObject _lock_Rickshaw;

	[Header("Bus :")] 
	[SerializeField] private GameObject _dark_Bus;
	[SerializeField] private GameObject _lock_Bus;

	[Header("Ambulance :")] 
	[SerializeField] private GameObject _dark_Ambulance;
	[SerializeField] private GameObject _lock_Ambulance;
	#endregion

	#region Function
	private void OnEnable()
	{
		LevelStatus();
		
	}
	
	void LevelStatus()
	{
		if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Rickshaw) == 1)
		{
			_dim_Rickshaw.SetActive(false);
			_lock_Rickshaw.SetActive(false);
		}

		if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Bus) == 1)
		{
			_dark_Bus.SetActive(false);
			_lock_Bus.SetActive(false);
		}
		else
		{
			_dark_Bus.SetActive(true);
			_lock_Bus.SetActive(true);
		}
		
		if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Ambulance) == 1)
		{
			_dark_Ambulance.SetActive(false);
			_lock_Ambulance.SetActive(false);
		}
		else
		{
			_dark_Ambulance.SetActive(true);
			_lock_Ambulance.SetActive(true);
		}
		

	}
	
	#endregion
        
    }
}


