using System;
using UnityEngine;
using UnityEngine.UI;

namespace FXnRXn
{
    public class UIPlayLevelSC : MonoBehaviour
    {
	#region Variable

	[Header("Level 1:")] 
	[SerializeField] private GameObject _dark_level1;
	[SerializeField] private GameObject _lock_level1;

	[Header("Level 2:")] 
	[SerializeField] private GameObject _dark_level2;
	[SerializeField] private GameObject _lock_level2;

	[Header("Level 3:")] 
	[SerializeField] private GameObject _dark_level3;
	[SerializeField] private GameObject _lock_level3;

	#endregion

	#region Function

	private void OnEnable()
	{
		LevelStatus();
		
	}

	void LevelStatus()
	{
		if (PlayerPrefs.GetInt(GlobalData.playLevel_lock1) == 1)
		{
			_dark_level1.SetActive(false);
			_lock_level1.SetActive(false);
		}

		if (PlayerPrefs.GetInt(GlobalData.playLevel_lock2) == 1)
		{
			_dark_level2.SetActive(false);
			_lock_level2.SetActive(false);
		}
		else
		{
			_dark_level2.SetActive(true);
			_lock_level2.SetActive(true);
		}
		
		if (PlayerPrefs.GetInt(GlobalData.playLevel_lock3) == 1)
		{
			_dark_level3.SetActive(false);
			_lock_level3.SetActive(false);
		}
		else
		{
			_dark_level3.SetActive(true);
			_lock_level3.SetActive(true);
		}
	}
	
	

	#endregion

    }
}


