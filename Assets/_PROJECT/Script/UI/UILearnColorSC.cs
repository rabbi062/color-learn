using UnityEngine;

namespace FXnRXn
{
    public class UILearnColorSC : MonoBehaviour
    {
	#region Variable
	[Header("Primary :")] 
	[SerializeField] private GameObject _dim_primary;
	[SerializeField] private GameObject _lock_primary;

	[Header("Secondary :")] 
	[SerializeField] private GameObject _dark_secondary;
	[SerializeField] private GameObject _lock_secondary;

	[Header("Tertiary :")] 
	[SerializeField] private GameObject _dark_tertiary;
	[SerializeField] private GameObject _lock_tertiary;
	
	#endregion

	#region Function
	
	private void OnEnable()
	{
		LevelStatus();
		
	}

	void LevelStatus()
	{
		if (PlayerPrefs.GetInt(GlobalData.learnColor_primary) == 1)
		{
			_dim_primary.SetActive(false);
			_lock_primary.SetActive(false);
		}

		if (PlayerPrefs.GetInt(GlobalData.learnColor_secondary) == 1)
		{
			_dark_secondary.SetActive(false);
			_lock_secondary.SetActive(false);
		}
		else
		{
			_dark_secondary.SetActive(true);
			_lock_secondary.SetActive(true);
		}
		
		if (PlayerPrefs.GetInt(GlobalData.learnColor_tertiary) == 1)
		{
			_dark_tertiary.SetActive(false);
			_lock_tertiary.SetActive(false);
		}
		else
		{
			_dark_tertiary.SetActive(true);
			_lock_tertiary.SetActive(true);
		}
	}
	
	
	#endregion
        
    }
}


