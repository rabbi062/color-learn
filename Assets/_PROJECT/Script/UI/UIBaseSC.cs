using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FXnRXn
{
	public abstract class UIBaseSC : MonoBehaviour
	{
		
		[Header("UI Panel :")] 
		public List<UI_Screen> _UIMenus;
		
		
		#region UI MANAGEMENT

		public virtual void ShowMenu(string name)
		{
			ShowMenuFunc(name, true);
		}
		
		public virtual void ShowMenu(string name , bool isAllUIDisable)
		{
			ShowMenuFunc(name, isAllUIDisable);
		}
		
		private void ShowMenuFunc(string name, bool disableAllScreens){
			if(disableAllScreens) MDisableAllScreens();

			foreach (UI_Screen UI in _UIMenus){
				if (UI.UI_name == name) {
                
					if (UI.UI_Object != null) {
						UI.UI_Object.SetActive(true);
                    

					} else {
						Debug.Log ("no menu found with name: " + name);
					}
				}
			}
		}
    
		public virtual void CloseMenu(string name){
			foreach (UI_Screen UI in _UIMenus){
				if (UI.UI_name == name)	UI.UI_Object.SetActive (false);
			}
		}
    
		public virtual void MDisableAllScreens(){
			foreach (UI_Screen UI in _UIMenus){ 
				if(UI.UI_Object != null) 
					UI.UI_Object.SetActive(false);
				else 
					Debug.Log("Null ref found in UI with name: " + UI.UI_name);
			}
		}
		
		
		
		

		#endregion
	}
	
	[System.Serializable]
	public class UI_Screen
	{
		public string UI_name;
		public GameObject UI_Object;
	}
}
