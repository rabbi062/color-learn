using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace FXnRXn
{
	public class UIMenuSC : UIBaseSC
	{

		#region Singleton

		public static UIMenuSC singleton;

		private void Awake()
		{
			if (singleton == null)
			{
				singleton = this;
			}
			else
			{
				Destroy(this);
			}
		}

		#endregion
		
		
		
		
   		#region Variable

        [SerializeField] private string _privacyUrl;

        [Header("----- Setting :")] 
        [SerializeField] private TMP_Text _userName;

        [Header("----- Buttons :")] 
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _aboutButton;
        [SerializeField] private Button _policyButton;
        [SerializeField] private Button _infoButton;
        [SerializeField] private Button _settingButton;
        [SerializeField] private Button _rankingButton;
        [SerializeField] private Button _shopButton;
        
        [Header("----- Panel Button")] 
        [SerializeField] private Button _settingCloseBtn;
        [SerializeField] private Button _rankingCloseBtn;
        [SerializeField] private Button _infoCloseBtn;
        [SerializeField] private Button _aboutCloseBtn;
        [SerializeField] private Button _shopCloseBtn;
        
        

        [Header("----- Level")] 
        [SerializeField] private Button _playLevel_learnColorBtn;
        [SerializeField] private Button _playLevel_paintingWithColorBtn;
        [SerializeField] private Button _playLevel_playingWithColorBtn;
        [Space(10)] 
        [SerializeField] private Button _playLevelBackBtn;
        [SerializeField] private Button _subLevel_learnColorBackBtn;
        [SerializeField] private Button _subLevel_paintingWithColorBackBtn;
        [SerializeField] private Button _subLevel_playingWithColorBackBtn;
        [Space(10)] 
        

        [Header("--------------------  :")] 
        public List<SubLevelButton> subLevelButtons = new List<SubLevelButton>();
        public List<LowerLevelButton> lowerLevelButtons = new List<LowerLevelButton>();



        private readonly string _settingPanel = "Setting";
        private readonly string _rankingPanel = "Ranking";
        private readonly string _infoPanel = "Info";
        private readonly string _aboutPanel = "About";
        private readonly string _shopPanel = "Shop";
        
        
        
        
        
        
        
        private readonly string _playLevelPanel = "PlayLevel";
        private readonly string _learnColorPanel = "Color Learn";
        private readonly string _paintingWithColorPanel = "PaintingWithColor";
        private readonly string _playingWithColorPanel = "PlayingWithColor";
        private readonly string _matchColorWithObject_LowerLevel = "Match Color With Object-Lower Level";
        private readonly string _parkingCar_LowerLevel = "Parking Car - Lower Level";

		#endregion

		#region Function
		
		

		private void Start()
		{
			if (!String.IsNullOrEmpty(PlayerPrefs.GetString(GlobalData.playerUsername)))
			{
				ShowUserName();
			}
			else
			{
				_userName.text = "User";
			}
			
			//Play Sound
			if (SoundManager.singleton != null)
			{
				SoundManager.singleton.PlaySound(SoundType.MenuBGSound, true);
			}
			

			ButtonPress();
			SubLevelButtonPress();

			LearnColorButtonPress();
			PlayWithColorButtonPress();
			PaintingWithColorButtonPress();
			LowerLevel_MatchColorsWithObjectButtonPress();
			LowerLevel_ParkingCarButtonPress();
		}

		
		void ButtonPress()
		{
			_playButton.onClick.RemoveAllListeners();
			_playButton.onClick.AddListener(() =>
			{
				ShowMenu(_playLevelPanel);
			});
			
			_playLevelBackBtn.onClick.RemoveAllListeners();
			_playLevelBackBtn.onClick.AddListener(() =>
			{
				CloseMenu(_playLevelPanel);
			});
			
			
			
			_exitButton.onClick.RemoveAllListeners();
			_exitButton.onClick.AddListener(() =>
			{
				if (LevelManager.singleton != null)
				{
					LevelManager.singleton.AppQuit();
					if (SoundManager.singleton != null)
					{
						if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
						{
							SoundManager.singleton.StopSound(SoundType.MenuBGSound);
						}
					}
				}
			});
			
			_aboutButton.onClick.RemoveAllListeners();
			_aboutButton.onClick.AddListener(() =>
			{
				ShowMenu(_aboutPanel);
			});
			
			_aboutCloseBtn.onClick.RemoveAllListeners();
			_aboutCloseBtn.onClick.AddListener(() =>
			{
				CloseMenu(_aboutPanel);
			});
			
			_policyButton.onClick.RemoveAllListeners();
			_policyButton.onClick.AddListener(() =>
			{
				if (!string.IsNullOrEmpty(_privacyUrl))
				{
					Application.OpenURL(_privacyUrl);
					if (SoundManager.singleton != null)
					{
						if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
						{
							SoundManager.singleton.StopSound(SoundType.MenuBGSound);
						}
					}
				}
			});
			
			_infoButton.onClick.RemoveAllListeners();
			_infoButton.onClick.AddListener(() =>
			{
				ShowMenu(_infoPanel);
			});
			
			_infoCloseBtn.onClick.RemoveAllListeners();
			_infoCloseBtn.onClick.AddListener(() =>
			{
				CloseMenu(_infoPanel);
			});
			
			_settingButton.onClick.RemoveAllListeners();
			_settingButton.onClick.AddListener(() =>
			{
				ShowMenu(_settingPanel);
			});
			
			_settingCloseBtn.onClick.RemoveAllListeners();
			_settingCloseBtn.onClick.AddListener(() =>
			{
				CloseMenu(_settingPanel);
			});
			
			_rankingButton.onClick.RemoveAllListeners();
			_rankingButton.onClick.AddListener(() =>
			{
				ShowMenu(_rankingPanel);
			});
			_rankingCloseBtn.onClick.RemoveAllListeners();
			_rankingCloseBtn.onClick.AddListener(() =>
			{
				CloseMenu(_rankingPanel);
			});
			
			
			
			_shopButton.onClick.RemoveAllListeners();
			_shopButton.onClick.AddListener(() =>
			{
				ShowMenu(_shopPanel);
			});
			
			_shopCloseBtn.onClick.RemoveAllListeners();
			_shopCloseBtn.onClick.AddListener(() =>
			{
				CloseMenu(_shopPanel);
			});
			
		}

		void SubLevelButtonPress()
		{
			// Learn Color
			_playLevel_learnColorBtn.onClick.RemoveAllListeners();
			_playLevel_learnColorBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.playLevel_lock1) == 1)
				{
					ShowMenu(_learnColorPanel);
				}
				
			});
			_subLevel_learnColorBackBtn.onClick.RemoveAllListeners();
			_subLevel_learnColorBackBtn.onClick.AddListener(() =>
			{
				CloseMenu(_learnColorPanel);
				ShowMenu(_playLevelPanel);
			});
			
			
			// Painting With Color
			_playLevel_paintingWithColorBtn.onClick.RemoveAllListeners();
			_playLevel_paintingWithColorBtn.onClick.AddListener(() =>
			{
				
				if (PlayerPrefs.GetInt(GlobalData.playLevel_lock2) == 1)
				{
					ShowMenu(_paintingWithColorPanel);
				}
				
			});
			_subLevel_paintingWithColorBackBtn.onClick.RemoveAllListeners();
			_subLevel_paintingWithColorBackBtn.onClick.AddListener(() =>
			{
				CloseMenu(_paintingWithColorPanel);
				ShowMenu(_playLevelPanel);
			});
			
			
			
			//Playing with color
			_playLevel_playingWithColorBtn.onClick.RemoveAllListeners();
			_playLevel_playingWithColorBtn.onClick.AddListener(() =>
			{
				
				if (PlayerPrefs.GetInt(GlobalData.playLevel_lock3) == 1)
				{
					ShowMenu(_playingWithColorPanel);
				}
				
			});
			_subLevel_playingWithColorBackBtn.onClick.RemoveAllListeners();
			_subLevel_playingWithColorBackBtn.onClick.AddListener(() =>
			{
				CloseMenu(_playingWithColorPanel);
				ShowMenu(_playLevelPanel);
			});
		}

		void LearnColorButtonPress()
		{
			subLevelButtons[0]._primaryColorBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._primaryColorBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.learnColor_primary) == 1)
				{
					if (!string.IsNullOrEmpty(subLevelButtons[0]._primaryColorScene))
					{
						if (LevelManager.singleton != null)
						{
							if (SoundManager.singleton != null)
							{
								if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
								{
									SoundManager.singleton.StopSound(SoundType.MenuBGSound);
								}
							}
							LevelManager.singleton.LoadLevel(subLevelButtons[0]._primaryColorScene);
						}
					}
				}
			});
			
			subLevelButtons[0]._secondaryColorBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._secondaryColorBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.learnColor_secondary) == 1)
				{
					if (!string.IsNullOrEmpty(subLevelButtons[0]._secondaryColorScene))
					{
						if (LevelManager.singleton != null)
						{
							if (SoundManager.singleton != null)
							{
								if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
								{
									SoundManager.singleton.StopSound(SoundType.MenuBGSound);
								}
							}
							LevelManager.singleton.LoadLevel(subLevelButtons[0]._secondaryColorScene);
						}
					}
				}
			});
			
			subLevelButtons[0]._tertiaryColorBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._tertiaryColorBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.learnColor_tertiary) == 1)
				{
					if (!string.IsNullOrEmpty(subLevelButtons[0]._tertiaryColorScene))
					{
						if (LevelManager.singleton != null)
						{
							if (SoundManager.singleton != null)
							{
								if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
								{
									SoundManager.singleton.StopSound(SoundType.MenuBGSound);
								}
							}
							LevelManager.singleton.LoadLevel(subLevelButtons[0]._tertiaryColorScene);
						}
					}
				}
			});
		}
		
		void PlayWithColorButtonPress()
		{
			subLevelButtons[0]._matchColorWithObjectBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._matchColorWithObjectBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.playWithColor_MatchColorWithObject) == 1)
				{
					ShowMenu(_matchColorWithObject_LowerLevel);
				}
			});
			subLevelButtons[0]._lowerLevel_matchWithColorObjectBackBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._lowerLevel_matchWithColorObjectBackBtn.onClick.AddListener(() =>
			{
				CloseMenu(_matchColorWithObject_LowerLevel);
				ShowMenu(_playingWithColorPanel);
			});
			
			subLevelButtons[0]._parkingColorBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._parkingColorBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.playWithColor_ParkingCars) == 1)
				{
					ShowMenu(_parkingCar_LowerLevel);
				}
			});
			
			subLevelButtons[0]._lowerLevel_parkingCarBackBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._lowerLevel_parkingCarBackBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.playWithColor_ParkingCars) == 1)
				{
					CloseMenu(_parkingCar_LowerLevel);
					ShowMenu(_playingWithColorPanel);
				}
			});
		}
		
		void PaintingWithColorButtonPress()
		{
			subLevelButtons[0]._alphabetBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._alphabetBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.paintingWithColor_Alphabet) == 1)
				{
					if (!string.IsNullOrEmpty(subLevelButtons[0]._alphabetScene))
					{
						if (LevelManager.singleton != null)
						{
							if (SoundManager.singleton != null)
							{
								if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
								{
									SoundManager.singleton.StopSound(SoundType.MenuBGSound);
								}
							}
							
							PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.paintingWithColor_Bangla, 1);
							PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.paintingWithColor_DrawingAndPainting, 1);
							
							LevelManager.singleton.LoadLevel(subLevelButtons[0]._alphabetScene);
							
							
						}
					}
					
					
				}
			});
			
			subLevelButtons[0]._banglaBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._banglaBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.paintingWithColor_Bangla) == 1)
				{
					if (!string.IsNullOrEmpty(subLevelButtons[0]._banglaScene))
					{
						if (LevelManager.singleton != null)
						{
							if (SoundManager.singleton != null)
							{
								if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
								{
									SoundManager.singleton.StopSound(SoundType.MenuBGSound);
								}
							}
							LevelManager.singleton.LoadLevel(subLevelButtons[0]._banglaScene);
						}
					}
				}
			});
			
			subLevelButtons[0]._drawingAndPaintingBtn.onClick.RemoveAllListeners();
			subLevelButtons[0]._drawingAndPaintingBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.paintingWithColor_DrawingAndPainting) == 1)
				{
					if (!string.IsNullOrEmpty(subLevelButtons[0]._drawingAndPaintingScene))
					{
						if (LevelManager.singleton != null)
						{
							if (SoundManager.singleton != null)
							{
								if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
								{
									SoundManager.singleton.StopSound(SoundType.MenuBGSound);
								}
							}
							LevelManager.singleton.LoadLevel(subLevelButtons[0]._drawingAndPaintingScene);
						}
					}
				}
			});
		}


		void LowerLevel_MatchColorsWithObjectButtonPress()
		{
			lowerLevelButtons[0]._AppleBtn.onClick.RemoveAllListeners();
			lowerLevelButtons[0]._AppleBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Apple) == 1)
				{
					if (LevelManager.singleton != null)
					{
						if(string.IsNullOrEmpty(lowerLevelButtons[0]._AppleScene)) return;
						if (SoundManager.singleton != null)
						{
							if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
							{
								SoundManager.singleton.StopSound(SoundType.MenuBGSound);
							}
						}
						
						LevelManager.singleton.LoadLevel(lowerLevelButtons[0]._AppleScene);
					}
				}
			});
			
			lowerLevelButtons[0]._BananaBtn.onClick.RemoveAllListeners();
			lowerLevelButtons[0]._BananaBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Banana) == 1)
				{
					if (LevelManager.singleton != null)
					{
						if(string.IsNullOrEmpty(lowerLevelButtons[0]._BananaScene)) return;
						
						if (SoundManager.singleton != null)
						{
							if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
							{
								SoundManager.singleton.StopSound(SoundType.MenuBGSound);
							}
						}
						LevelManager.singleton.LoadLevel(lowerLevelButtons[0]._BananaScene);
					}
				}
			});
			
			lowerLevelButtons[0]._GrapeBtn.onClick.RemoveAllListeners();
			lowerLevelButtons[0]._GrapeBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Grape) == 1)
				{
					if (LevelManager.singleton != null)
					{
						if(string.IsNullOrEmpty(lowerLevelButtons[0]._GrapeScene)) return;
						
						if (SoundManager.singleton != null)
						{
							if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
							{
								SoundManager.singleton.StopSound(SoundType.MenuBGSound);
							}
						}
						LevelManager.singleton.LoadLevel(lowerLevelButtons[0]._GrapeScene);
					}
				}
			});
			
			
			
			lowerLevelButtons[0]._MangoBtn.onClick.RemoveAllListeners();
			lowerLevelButtons[0]._MangoBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Mango) == 1)
				{
					if (LevelManager.singleton != null)
					{
						if(string.IsNullOrEmpty(lowerLevelButtons[0]._MangoScene)) return;
						
						if (SoundManager.singleton != null)
						{
							if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
							{
								SoundManager.singleton.StopSound(SoundType.MenuBGSound);
							}
						}
						LevelManager.singleton.LoadLevel(lowerLevelButtons[0]._MangoScene);
					}
				}
			});
			
			
			
			
			
			
		}

		void LowerLevel_ParkingCarButtonPress()
		{
			lowerLevelButtons[0]._RickshawBtn.onClick.RemoveAllListeners();
			lowerLevelButtons[0]._RickshawBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Rickshaw) == 1)
				{
					if (LevelManager.singleton != null)
					{
						if(string.IsNullOrEmpty(lowerLevelButtons[0]._RickshawScene)) return;
						
						if (SoundManager.singleton != null)
						{
							if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
							{
								SoundManager.singleton.StopSound(SoundType.MenuBGSound);
							}
						}
						LevelManager.singleton.LoadLevel(lowerLevelButtons[0]._RickshawScene);
					}
				}
			});
			
			lowerLevelButtons[0]._BusBtn.onClick.RemoveAllListeners();
			lowerLevelButtons[0]._BusBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Bus) == 1)
				{
					if (LevelManager.singleton != null)
					{
						if(string.IsNullOrEmpty(lowerLevelButtons[0]._BusScene)) return;
						
						if (SoundManager.singleton != null)
						{
							if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
							{
								SoundManager.singleton.StopSound(SoundType.MenuBGSound);
							}
						}
						LevelManager.singleton.LoadLevel(lowerLevelButtons[0]._BusScene);
					}
				}
			});
			
			lowerLevelButtons[0]._AmbulanceBtn.onClick.RemoveAllListeners();
			lowerLevelButtons[0]._AmbulanceBtn.onClick.AddListener(() =>
			{
				if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Ambulance) == 1)
				{
					if (LevelManager.singleton != null)
					{
						if(string.IsNullOrEmpty(lowerLevelButtons[0]._AmbulanceScene)) return;
						
						if (SoundManager.singleton != null)
						{
							if (SoundManager.singleton.IsSoundPlaying(SoundType.MenuBGSound))
							{
								SoundManager.singleton.StopSound(SoundType.MenuBGSound);
							}
						}
						LevelManager.singleton.LoadLevel(lowerLevelButtons[0]._AmbulanceScene);
					}
				}
			});
		}
		
		
		
		
		

		public void ShowUserName()
		{
			_userName.text = PlayerPrefs.GetString(GlobalData.playerUsername).ToString();
		}

		#endregion
	}

	[System.Serializable]
	public class SubLevelButton
	{
		[Header("----- Learn Color Button :")] 
		public Button _primaryColorBtn;
		public string _primaryColorScene;
		public Button _secondaryColorBtn;
		public string _secondaryColorScene;
		public Button _tertiaryColorBtn;
		public string _tertiaryColorScene;
		
		[Header("----- Play With Color Button :")] 
		public Button _matchColorWithObjectBtn;
		public Button _lowerLevel_matchWithColorObjectBackBtn;
		public Button _parkingColorBtn;
		public Button _lowerLevel_parkingCarBackBtn;
		
		
		[Header("----- Painting With Color Button :")]
		public Button _alphabetBtn;
		public string _alphabetScene;
		
		public Button _banglaBtn;
		public string _banglaScene;
		
		public Button _drawingAndPaintingBtn;
		public string _drawingAndPaintingScene;
	}
	
	[System.Serializable]
	public class LowerLevelButton
	{
		[Header("----- Match Color With Object :")] 
		public Button _AppleBtn;
		public string _AppleScene;
		public Button _BananaBtn;
		public string _BananaScene;
		public Button _GrapeBtn;
		public string _GrapeScene;
		public Button _MangoBtn;
		public string _MangoScene;
		
		[Header("----- Parking Car :")] 
		public Button _RickshawBtn;
		public string _RickshawScene;
		public Button _BusBtn;
		public string _BusScene;
		public Button _AmbulanceBtn;
		public string _AmbulanceScene;


	}
}
