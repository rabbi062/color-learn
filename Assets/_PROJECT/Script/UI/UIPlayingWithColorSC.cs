using UnityEngine;

namespace FXnRXn
{
    public class UIPlayingWithColorSC : MonoBehaviour
    {
	#region Variable
	[Header("MatchColorWithObject :")] 
	[SerializeField] private GameObject _dim_MatchColorWithObject;
	[SerializeField] private GameObject _lock_MatchColorWithObject;

	[Header("ParkingCars :")] 
	[SerializeField] private GameObject _dark_ParkingCars;
	[SerializeField] private GameObject _lock_ParkingCars;
	
	
	#endregion

	#region Function
	
	private void OnEnable()
	{
		LevelStatus();
		
	}

	void LevelStatus()
	{
		if (PlayerPrefs.GetInt(GlobalData.playWithColor_MatchColorWithObject) == 1)
		{
			_dim_MatchColorWithObject.SetActive(false);
			_lock_MatchColorWithObject.SetActive(false);
		}

		if (PlayerPrefs.GetInt(GlobalData.playWithColor_ParkingCars) == 1)
		{
			_dark_ParkingCars.SetActive(false);
			_lock_ParkingCars.SetActive(false);
		}
		else
		{
			_dark_ParkingCars.SetActive(true);
			_lock_ParkingCars.SetActive(true);
		}
		
	}
	
	#endregion
        
    }
}


