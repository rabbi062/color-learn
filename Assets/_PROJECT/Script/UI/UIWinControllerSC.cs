using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FXnRXn
{
	public enum LevelSystem
	{
		_1_1,
		_1_2,
		_1_3,
		_2_1_1,
		_2_1_2,
		_2_1_3,
		_2_1_4,
		_2_2_1,
		_2_2_2,
		_2_2_3,
		Alphabet,
		AlphabetEnd,
		Bangla,
		BanglaEnd
	}
	
	
	
	
    public class UIWinControllerSC : MonoBehaviour
    {
	#region Variable

	[SerializeField] private LevelSystem _levelSystem;

	[Header("---- Setting :")] 
	[SerializeField] private string nextLevel;
	
	// Star
	[Header("---- Star :")] 
	[SerializeField] private List<GameObject> starObj;

	// Timer
	[Header("---- Timer :")] 
	[SerializeField] private TMP_Text timerText;
	
	// Chocolate
	[Header("---- Chocolate :")] 
	[SerializeField] private TMP_Text chocolateText;
	
	// Button
	[Header("---- Buttonj:")] 
	[SerializeField] private Button _replayBtn;
	[SerializeField] private Button _nextBtn;
	[SerializeField] private Button _homeBtn;
	
	#endregion

	#region Function

	private void Start()
	{
		ButtonPress();
	}

	void ButtonPress()
	{
		_replayBtn.onClick.RemoveAllListeners();
		_replayBtn.onClick.AddListener(() =>
		{
			if (LevelManager.singleton != null)
			{
				LevelManager.singleton.LoadLevel(SceneManager.GetActiveScene().name);
			}
			
			
			PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.timer, 0);
		});
		
		_nextBtn.onClick.RemoveAllListeners();
		_nextBtn.onClick.AddListener(() =>
		{
			
			switch (_levelSystem)
			{
				case LevelSystem._1_1:
					if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.learnColor_secondary,0) == 1)
					{
						if (LevelManager.singleton != null)
						{
							LevelManager.singleton.LoadLevel(nextLevel);
						}
					}
				
					break;
				case LevelSystem._1_2:
					if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.learnColor_tertiary,0) == 1)
					{
						if (LevelManager.singleton != null)
						{
							LevelManager.singleton.LoadLevel(nextLevel);
						}
					}
					break;
			
				case LevelSystem._1_3:
					if (LevelManager.singleton != null)
					{
						LevelManager.singleton.LoadLevel(nextLevel);
					}
					break;
				case LevelSystem._2_1_1:
					if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.lowerLevel_Banana,0) == 1)
					{
						if (LevelManager.singleton != null)
						{
							LevelManager.singleton.LoadLevel(nextLevel);
						}
					}
					break;
				
				case LevelSystem._2_1_2:
					if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.lowerLevel_Grape,0) == 1)
					{
						if (LevelManager.singleton != null)
						{
							LevelManager.singleton.LoadLevel(nextLevel);
						}
					}
					break;
				
				case LevelSystem._2_1_3:
					if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.lowerLevel_Mango,0) == 1)
					{
						if (LevelManager.singleton != null)
						{
							LevelManager.singleton.LoadLevel(nextLevel);
						}
					}
					break;
				
				case LevelSystem._2_1_4:
					if (LevelManager.singleton != null)
					{
						LevelManager.singleton.LoadLevel(nextLevel);
					}
					break;
				
				case LevelSystem._2_2_1:
					if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.lowerLevel_Bus,0) == 1)
					{
						if (LevelManager.singleton != null)
						{
							LevelManager.singleton.LoadLevel(nextLevel);
						}
					}
					break;
				
				case LevelSystem._2_2_2:
					if (PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.lowerLevel_Ambulance,0) == 1)
					{
						if (LevelManager.singleton != null)
						{
							LevelManager.singleton.LoadLevel(nextLevel);
						}
					}
					break;
				
				case LevelSystem._2_2_3:
					if (LevelManager.singleton != null)
					{
						LevelManager.singleton.LoadLevel(nextLevel);
					}
					break;
				
				case LevelSystem.Alphabet:
					int temp = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.alphabetScene, 0);
					temp += 1;
					PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.alphabetScene, temp);
					if (LevelManager.singleton != null)
					{
						LevelManager.singleton.LoadLevel(nextLevel);
					}
					
					break;
				
				case LevelSystem.AlphabetEnd:
					PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.alphabetScene, 1);
					if (LevelManager.singleton != null)
					{
						LevelManager.singleton.LoadLevel(nextLevel);
					}
					break;
				
				case LevelSystem.Bangla:
					int tempb = PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.banglaScene, 0);
					tempb += 1;
					PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.banglaScene, tempb);
					if (LevelManager.singleton != null)
					{
						LevelManager.singleton.LoadLevel(nextLevel);
					}
					break;
				
				case LevelSystem.BanglaEnd:
					PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.banglaScene, 1);
					if (LevelManager.singleton != null)
					{
						LevelManager.singleton.LoadLevel(nextLevel);
					}
					break;
				
				
				
				
				
				
				default:
					throw new ArgumentOutOfRangeException();
			}
			
			
			
			
			PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.timer, 0);
		});
		
		_homeBtn.onClick.RemoveAllListeners();
		_homeBtn.onClick.AddListener(() =>
		{
			
			if (LevelManager.singleton != null)
			{
				LevelManager.singleton.LoadLevel("Menu");
			}
			
			PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.timer, 0);
		});
	}

	private void OnEnable()
	{
		DisplayStar(PlayerPrefsUtility.GetPlayerPrefsFloat(GlobalData.timer, 0));
		float t =  PlayerPrefsUtility.GetPlayerPrefsFloat(GlobalData.timer, 0);
		DisplayTime(t);
		LevelState();
	}



	void LevelState()
	{
		switch (_levelSystem)
		{
			case LevelSystem._1_1:
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.learnColor_secondary, 1);
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.playLevel_lock2, 1);
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.playLevel_lock3, 1);
				break;
			case LevelSystem._1_2:
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.learnColor_tertiary, 1);
				break;
			case LevelSystem._1_3:
				
				break;
			
			case LevelSystem._2_1_1:
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.playWithColor_ParkingCars, 1);
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.lowerLevel_Banana, 1);
				break;
			case LevelSystem._2_1_2:
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.lowerLevel_Grape, 1);
				break;
			case LevelSystem._2_1_3:
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.lowerLevel_Mango, 1);
				break;
			case LevelSystem._2_1_4:
				break;
			
			case LevelSystem._2_2_1:
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.lowerLevel_Bus, 1);
				break;
			case LevelSystem._2_2_2:
				PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.lowerLevel_Ambulance, 1);
				break;
			case LevelSystem._2_2_3:
				break;
			
			
			
			default:
				throw new ArgumentOutOfRangeException();
		}
	}
	
	
	
	
	
	
	
	
	

	void DisplayStar(float t)
	{
		if (t < 180)
		{
			starObj[0].SetActive(true);
			starObj[1].SetActive(true);
			starObj[2].SetActive(true);
			CalculateChocolate(3);
		}

		if (t >= 180 && t < 240)
		{
			starObj[0].SetActive(true);
			starObj[1].SetActive(true);
			starObj[2].SetActive(false);
			CalculateChocolate(2);
		}
		if (t >= 240 && t < 300)
		{
			starObj[0].SetActive(true);
			starObj[1].SetActive(false);
			starObj[2].SetActive(false);
			CalculateChocolate(1);
		}
		if (t >= 300)
		{
			starObj[0].SetActive(false);
			starObj[1].SetActive(false);
			starObj[2].SetActive(false);
			CalculateChocolate(0);
		}
	}

	void CalculateChocolate(int c)
	{
		chocolateText.text = c.ToString();
		ChocolateSystem.AddChocolate(c);
	}


	void DisplayTime(float timeToDisplay)
	{

		float minutes = Mathf.FloorToInt(timeToDisplay / 60); 
		float seconds = Mathf.FloorToInt(timeToDisplay % 60);

		timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
	}
	
	#endregion
        
    }
}


