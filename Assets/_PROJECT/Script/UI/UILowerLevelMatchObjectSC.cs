using UnityEngine;

namespace FXnRXn
{
    public class UILowerLevelMatchObjectSC : MonoBehaviour
    {
	#region Variable
	[Header("Apple :")] 
	[SerializeField] private GameObject _dim_Apple;
	[SerializeField] private GameObject _lock_Apple;

	[Header("Banana :")] 
	[SerializeField] private GameObject _dark_Banana;
	[SerializeField] private GameObject _lock_Banana;

	[Header("Grape :")] 
	[SerializeField] private GameObject _dark_Grape;
	[SerializeField] private GameObject _lock_Grape;
	
	[Header("Mango :")] 
	[SerializeField] private GameObject _dark_Mango;
	[SerializeField] private GameObject _lock_Mango;
	
	#endregion

	#region Function
	
	private void OnEnable()
	{
		LevelStatus();
		
	}

	void LevelStatus()
	{
		if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Apple) == 1)
		{
			_dim_Apple.SetActive(false);
			_lock_Apple.SetActive(false);
		}

		if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Banana) == 1)
		{
			_dark_Banana.SetActive(false);
			_lock_Banana.SetActive(false);
		}
		else
		{
			_dark_Banana.SetActive(true);
			_lock_Banana.SetActive(true);
		}
		
		if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Grape) == 1)
		{
			_dark_Grape.SetActive(false);
			_lock_Grape.SetActive(false);
		}
		else
		{
			_dark_Grape.SetActive(true);
			_lock_Grape.SetActive(true);
		}
		
		if (PlayerPrefs.GetInt(GlobalData.lowerLevel_Mango) == 1)
		{
			_dark_Mango.SetActive(false);
			_lock_Mango.SetActive(false);
		}
		else
		{
			_dark_Mango.SetActive(true);
			_lock_Mango.SetActive(true);
		}
	}
	
	#endregion
        
    }
}


