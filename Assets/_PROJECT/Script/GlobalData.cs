using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FXnRXn
{
	public static class GlobalData 
	{
		public static string gameFirstTimeOpen = "Game First Time Open";
		public static string playerUsername = "username";
		public static string noAds = "No Ad"; // 0 show ad, 1 no ad

		public static string chocolate = "Chocolate";
		public static string playerClass = "Player Class";

		public static string timer = "Timer";
		public static string timerState = "Timer State";
		
		
		
		// Playerprefs - Painting with color
		public static string banglaScene = "Bangla Scene";
		public static string alphabetScene = "Alphabet Scene";
		
		
		// Level
		public static string playLevel_lock1 = "PlayLevel Lock 1";
		public static string playLevel_lock2 = "PlayLevel Lock 2";
		public static string playLevel_lock3 = "PlayLevel Lock 3";
		
		//Learn Color
		public static string learnColor_primary = "learnColor primary";
		public static string learnColor_secondary = "learnColor secondary";
		public static string learnColor_tertiary = "learnColor tertiary";
		
		//Play with Color
		public static string playWithColor_MatchColorWithObject = "PlayWithColor MatchColorWithObject";
		//---------------------------------
		public static string lowerLevel_Apple = "Lower Level Apple";
		public static string lowerLevel_Banana = "Lower Level Banana";
		public static string lowerLevel_Grape = "Lower Level Grape";
		public static string lowerLevel_Mango = "Lower Level Mango";
		
		public static string playWithColor_ParkingCars = "PlayWithColor ParkingCars";
		//-----------------------------------
		public static string lowerLevel_Rickshaw = "Lower Level Rickshaw";
		public static string lowerLevel_Bus = "Lower Level Bus";
		public static string lowerLevel_Ambulance = "Lower Level Ambulance";
		
		//Painting with Color
		public static string paintingWithColor_Alphabet = "PaintingWithColor Alphabet";
		public static string paintingWithColor_Bangla = "PaintingWithColor Bangla";
		public static string paintingWithColor_DrawingAndPainting = "PaintingWithColor DrawingAndPainting";
	}
}
