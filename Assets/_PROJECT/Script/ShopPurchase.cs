using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;


namespace FXnRXn
{
	public class ShopPurchase : MonoBehaviour
	{
   		#region Variable
        private string noAd = "com.kp.ColorsGames.noAd";
		#endregion

		#region Function

		public void OnPurchaseComplete(Product product)
		{
			if (product.definition.id == noAd)
			{
				PlayerPrefs.SetInt(GlobalData.noAds, 1);
			}
		}
		
		public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
		{
			Debug.Log(failureReason);
		}
		

		#endregion
	}
}
