using System;
using UnityEngine;

/// ----------------------------------------
/// int HighScore = PlayerPrefsSettingUtility.SettingUtil.GetPlayerPrefsInt(key, 0);
/// string key = "ThisApp_HighScore";
/// PlayerPrefsUtility. SetPlayerPrefsInt(key, HighScore);
/// -----------------------------------------


namespace FXnRXn
{
    public static class PlayerPrefsUtility
    {
        
        //=========== string ==================

        #region String

        public static string GetPlayerPrefsString(string keyName, string defaultValue)
        {
            
            if (!PlayerPrefs.HasKey(keyName))
            {
                PlayerPrefs.SetString(keyName, defaultValue);
                PlayerPrefs.Save();
            }
            return PlayerPrefs.GetString(keyName, defaultValue);
        }

        public static void SetPlayerPrefsString(string keyName, string setValue)
        {
            
            PlayerPrefs.SetString(keyName, setValue);
            PlayerPrefs.Save();
        }

        #endregion
        
        //=========== float ==================

        #region Float

        public static float GetPlayerPrefsFloat(string keyName, float defaultValue)
        {
            
            if (!PlayerPrefs.HasKey(keyName))
            {
                PlayerPrefs.SetFloat(keyName, defaultValue);
                PlayerPrefs.Save();
            }
            return PlayerPrefs.GetFloat(keyName, defaultValue);
        }

        public static void SetPlayerPrefsFloat(string keyName, float setValue)
        {
            
            PlayerPrefs.SetFloat(keyName, setValue);
            PlayerPrefs.Save();
        }

        #endregion
        
        //=========== int ==================

        #region Int

        public static int GetPlayerPrefsInt(string keyName, int defaultValue)
        {
            
            if (!PlayerPrefs.HasKey(keyName))
            {
                PlayerPrefs.SetInt(keyName, defaultValue);
                PlayerPrefs.Save();
            }
            return PlayerPrefs.GetInt(keyName, defaultValue);
        }

        public static void SetPlayerPrefsInt(string keyName, int setValue)
        {
            
            PlayerPrefs.SetInt(keyName, setValue);
            PlayerPrefs.Save();
        }

        #endregion
        
        //=========== bool(int custom) ==============

        #region bool(int custom)

        public static bool GetPlayerPrefsBool(string keyName, bool defaultValue)
        {

            int boolPara = (defaultValue == true) ? 0 : -1;

            if (!PlayerPrefs.HasKey(keyName))
            {
                PlayerPrefs.SetInt(keyName, boolPara);
                PlayerPrefs.Save();
            }

            if (PlayerPrefs.GetInt(keyName) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void SetPlayerPrefsBool(string keyName, bool setValue)
        {
            
            int boolPara = (setValue == true) ? 0 : -1;
            PlayerPrefs.SetInt(keyName, boolPara);
            PlayerPrefs.Save();
        }

        #endregion
        
        //=========== DateTime(string custom) ==================

        #region DateTime(string custom)

        public static DateTime GetPlayerPrefsDateTime(string keyName, DateTime defaultValue)
        {
            
            if (!PlayerPrefs.HasKey(keyName))
            {
                PlayerPrefs.SetString(keyName, defaultValue.ToString());
                PlayerPrefs.Save();
            }
            return DateTime.Parse(PlayerPrefs.GetString(keyName));
        }

        public static void SetPlayerPrefsDateTime(string keyName, DateTime setValue)
        {
          
            PlayerPrefs.SetString(keyName, setValue.ToString());
            PlayerPrefs.Save();
        }

        #endregion

        //=========== Vector3(string custom) ==================

        #region Vector3(string custom)

        public static Vector3 GetPlayerPrefsVector3(string keyName, Vector3 defaultValue)
        {
            
            if (!PlayerPrefs.HasKey(keyName))
            {
                PlayerPrefs.SetString(keyName,
                    defaultValue.x.ToString() + "," +
                    defaultValue.y.ToString() + "," +
                    defaultValue.z.ToString()
                );
                PlayerPrefs.Save();
            }
            
            string[] Vec3 = PlayerPrefs.GetString(keyName).Split(',');
            return new Vector3(float.Parse(Vec3[0]), float.Parse(Vec3[1]), float.Parse(Vec3[2]));
        }

        public static void SetPlayerPrefsVector3(string keyName, Vector3 Value)
        {
            
            PlayerPrefs.SetString(keyName, Value.x + "," + Value.y + "," + Value.z);
            PlayerPrefs.Save();
        }

        #endregion
        
        
    }
}


