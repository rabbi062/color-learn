using System;
using TMPro;
using UnityEngine;

namespace FXnRXn
{
    public class PlayerInfoManagerSC : MonoBehaviour
    {
	#region Variable

	[SerializeField] private TMP_Text _chocolateText;
	[SerializeField] private TMP_Text _playerClassText;


	#endregion

	#region Function

	private void OnEnable()
	{
		_chocolateText.text = ChocolateSystem.GetChocolate().ToString();
		_playerClassText.text = "Class " +PlayerClassSystem.GetPlayerClass().ToString();
	}

	#endregion

    }
    
    
    public static class ChocolateSystem
    {
	    public static void AddChocolate(int _)
	    {
		    int temp = _ + PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.chocolate, 0);
		    PlayerPrefsUtility.SetPlayerPrefsInt(GlobalData.chocolate, temp);
	    }

	    public static int GetChocolate()
	    {
		    return PlayerPrefsUtility.GetPlayerPrefsInt(GlobalData.chocolate, 0);
	    }
    }
    
    public static class PlayerClassSystem
    {
	    public static int GetPlayerClass()
	    {
		    int c = 0;
		    int choc = ChocolateSystem.GetChocolate();
		    if (choc >= 0 && choc < 699)
		    {
			    c = 1;
		    }
		    if (choc >= 700 && choc < 1499)
		    {
			    c = 2;
		    }
		    if (choc >= 1500 && choc < 2999)
		    {
			    c = 3;
		    }
		    if (choc >= 3000 && choc < 3999)
		    {
			    c = 4;
		    }
		    if (choc >= 4000 && choc < 5999)
		    {
			    c = 5;
		    }
		    if (choc >= 6000)
		    {
			    c = 6;
		    }

		    return c;

	    }
    
    }
    
}


