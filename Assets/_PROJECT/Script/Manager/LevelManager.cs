using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;



namespace FXnRXn
{
	public class LevelManager : MonoBehaviour
	{
		#region Singleton
        
		public static LevelManager singleton;

		private void Awake()
		{
			if (singleton == null)
			{
				singleton = this;
			}
			else
			{
				Destroy(this.gameObject);
			}
		}
		#endregion
		
		
   		

		#region Function
		
		public void LoadLevel(string scene)
		{
			LoadingSceneManager.LoadScene(scene);
		}

		public void AppQuit()
		{
			Application.Quit();
		}
		
		
		#endregion
	}
}
