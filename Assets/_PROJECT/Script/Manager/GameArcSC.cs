using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FXnRXn
{
	public class GameArcSC : MonoBehaviour
	{
   		#region Variable

        public static GameArcSC Singleton;

		#endregion

		#region Function

		private void Awake()
		{
			OnAwake();
			if (Singleton == null)
			{
				Singleton = this;
			}
			else
			{
				Destroy(this.gameObject);
			}
			DontDestroyOnLoad(this.gameObject);
		}
		

		void OnAwake()
		{
			
			if (!PlayerPrefs.HasKey(GlobalData.gameFirstTimeOpen))
			{
				PlayerPrefs.SetInt(GlobalData.gameFirstTimeOpen, 1);
			}
			
			
			if (!PlayerPrefs.HasKey(GlobalData.playLevel_lock1))
			{
				PlayerPrefs.SetInt(GlobalData.playLevel_lock1, 1);
			}
			if (!PlayerPrefs.HasKey(GlobalData.learnColor_primary))
			{
				PlayerPrefs.SetInt(GlobalData.learnColor_primary, 1);
			}
			if (!PlayerPrefs.HasKey(GlobalData.playWithColor_MatchColorWithObject))
			{
				PlayerPrefs.SetInt(GlobalData.playWithColor_MatchColorWithObject, 1);
			}
			if (!PlayerPrefs.HasKey(GlobalData.paintingWithColor_Alphabet))
			{
				PlayerPrefs.SetInt(GlobalData.paintingWithColor_Alphabet, 1);
			}
			
			if (!PlayerPrefs.HasKey(GlobalData.lowerLevel_Apple))
			{
				PlayerPrefs.SetInt(GlobalData.lowerLevel_Apple, 1);
			}
			if (!PlayerPrefs.HasKey(GlobalData.lowerLevel_Rickshaw))
			{
				PlayerPrefs.SetInt(GlobalData.lowerLevel_Rickshaw, 1);
			}

			if (!PlayerPrefs.HasKey(GlobalData.alphabetScene))
			{
				PlayerPrefs.SetInt(GlobalData.alphabetScene, 1);
			}
			if (!PlayerPrefs.HasKey(GlobalData.banglaScene))
			{
				PlayerPrefs.SetInt(GlobalData.banglaScene, 1);
			}
			
			
		}

		#endregion
	}
}
