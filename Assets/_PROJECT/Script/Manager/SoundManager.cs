using System;
using System.Collections.Generic;
using UnityEngine;

namespace FXnRXn
{
	public enum SoundType
	{
		ClickSound,
		MenuBGSound,
		Level1Sound
	}

	[System.Serializable]
	public class Sound
	{
		public SoundType soundType;
		public AudioSource sourceSound;
	}
	
    public class SoundManager : MonoBehaviour
    {
	    
	    #region Singleton

	    public static SoundManager singleton;

		private void Awake()
		{
			OnAwake();
			if (singleton == null)
			{
				singleton = this;
			}
			else
			{
				Destroy(this);
			}
		}

		#endregion
		
	    
		#region Variable

		[SerializeField] private List<Sound> sounds = new List<Sound>();
		private Dictionary<SoundType, Sound> soundDictionary = new Dictionary<SoundType, Sound>();


		#endregion

		#region Function

		void OnAwake()
		{
			for (int i = 0; i < sounds.Count; i++)
			{
				soundDictionary.Add(sounds[i].soundType, sounds[i]);
			}
		}


		private void OnEnable()
		{
			Switch.OnSettingSound += SoundSlider;
		}

		private void OnDisable()
		{
			Switch.OnSettingSound -= SoundSlider;
		}


		public void PlaySound(SoundType soundType, bool loop)
		{
			Sound temp;
			if (soundDictionary.TryGetValue(soundType, out temp))
			{
				temp.sourceSound.loop = loop;
				temp.sourceSound.Play();
			}
		}

		public void StopSound(SoundType soundType)
		{
			Sound temp;
			if (soundDictionary.TryGetValue(soundType, out temp))
			{
				temp.sourceSound.Stop();
			}
		}

		public void PlayOneShotSound(SoundType soundType)
		{
			Sound temp;
			if (soundDictionary.TryGetValue(soundType, out temp))
			{
				temp.sourceSound.PlayOneShot(temp.sourceSound.clip);
			}
		}

		public bool IsSoundPlaying(SoundType soundType)
		{
			bool isplaying = false;
			
			Sound temp;
			if (soundDictionary.TryGetValue(soundType, out temp))
			{
				isplaying = temp.sourceSound.isPlaying;
			}

			return isplaying;
		}



		public void SoundSlider(bool s)
		{
			foreach (var S in sounds)
			{
				S.sourceSound.mute = s;
			}
		}
		
		
		
		#endregion

    }
}


